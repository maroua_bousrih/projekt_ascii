
import java.util.Scanner;



import org.campus02.shape.ShapeA;
import org.campus02.shape.ShapeB;
import org.campus02.shape.ShapeC;
import org.campus02.shape.ShapeD;
import org.campus02.shape.ShapeE;
import org.campus02.shape.ShapeF;



public class DemoApp {
	
		public static void main(String[] args) {
			Scanner scan= new Scanner(System.in);
			System.out.println("give a Shape code from 1 to 6");
			int nbr = scan.nextInt();
			switch (nbr) {
			case 1:
				ShapeA xA = new ShapeA();
				xA.print();
				break;
			case 2: 
				ShapeB xB = new ShapeB();
				xB.print();
				break;
			case 3:
				ShapeC xC= new ShapeC();
				xC.print();
				break;
			case 4:
				ShapeD xD= new ShapeD();
				xD.print();	
				break;
			case 5: 
				ShapeE xE= new ShapeE();
				xE.print();
				break;
			case 6:
				ShapeF xF= new ShapeF();
				xF.print();
				break;
				
			default:
				System.out.println("Invalid Shape code");
				break;
			}
			
			
			scan.close();

//			System.out.println("Shape A and B");
//			ShapeA xA = new ShapeA();
//			xA.print();
//			System.out.println();
//			ShapeB xB = new ShapeB();
//			xB.print();
//
//
//			System.out.println("Shape C and D");
//
//
//			ShapeC xC= new ShapeC();
//			xC.print();
//			System.out.println();
//			ShapeD xD= new ShapeD();
//			xD.print();
//
//			System.out.println("Shape E and F");
//
//			ShapeE xE= new ShapeE();
//			xE.print();
//			ShapeF xF= new ShapeF();
//			xF.print();
			
			
			
			
			
		}

	}
